
const filesToCache = [
    "/",
    "https://pwa-lsth.onrender.com/public/manifest.json",
    "https://pwa-lsth.onrender.com/public/offline.html",
    "https://pwa-lsth.onrender.com/public/404.html"
]

const staticCacheName = 'static-cache'

self.addEventListener('install', event => {
    console.log('Service worker installing…');  
    event.waitUntil(
        caches.open(staticCacheName).then((cache) => {
            return cache.addAll(filesToCache)
        })
    )  
});

self.addEventListener('activate', event => {
    console.log('Service worker installing…');
    const cacheWhiteList = [staticCacheName]
    event.waitUntil(
        caches.keys().then((cachesNames) =>{
            return Promise.all(
                cachesNames.map((cacheName) => {
                    if(cacheWhiteList.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                })
            )
        })
    )
});

self.addEventListener('fetch', (event) => 
{        
    console.log("fetch")
    event.respondWith(
        caches
            .match(event.request)
            .then((response) => {
                if(response) {
                    return response;
                }
                return fetch(event.request).then((response) => {
                    if(response.status === 404){
                        return caches.match('404.html')
                    }
                    return caches.open(staticCacheName).then((cache) => {
                        if (!event.request.url.startsWith('chrome-extension://')) {
                            cache.put(event.request.url, response.clone());
                        }
                        return response;
                    })
                })
            }).catch((error) => {
                return caches.match("offline.html")
            })
    )
});


let getAllSnapshots = async function () {
    return new Promise((resolve, reject) => {
      var dbName = 'Image';
      var storeName = 'Images';
      var dbVersion = 1;
  
      var request = indexedDB.open(dbName, dbVersion);
  
      request.onsuccess = function (event) {
        var db = event.target.result;
  
        var transaction = db.transaction([storeName], 'readonly');
        var store = transaction.objectStore(storeName);
  
        var getAllRequest = store.getAll();
  
        getAllRequest.onsuccess = function (event) {
          var snapshots = event.target.result;
          resolve(snapshots);
        };
  
        getAllRequest.onerror = function (event) {
          reject('Greška pri dohvaćanju svih zapisa iz IndexedDB.');
        };
      };
  
      request.onerror = function (event) {
        reject('Greška pri otvaranju baze podataka.');
      };
    });
  };

  function deleteSnapshotById(databaseName, storeName, idToDelete) {
    return new Promise((resolve, reject) => {
        const openRequest = indexedDB.open(databaseName, 1);

        openRequest.onerror = function (event) {
            reject(new Error('Error opening database'));
        };

        openRequest.onsuccess = function (event) {
            const db = event.target.result;

            const transaction = db.transaction([storeName], 'readwrite');
            const store = transaction.objectStore(storeName);

            const deleteRequest = store.delete(idToDelete);

            deleteRequest.onsuccess = function (event) {
                resolve('Element successfully deleted');
            };

            deleteRequest.onerror = function (event) {
                reject(new Error('Error deleting element'));
            };

            transaction.oncomplete = function (event) {
                db.close();
            };
        };
    });
}
 

  let syncSnaps = async function(){
    console.log("Call the post method")
    try {
        var snapshots = await getAllSnapshots();
        const fetchPromises = snapshots.map(async(element) => {
            
            let jsonData = JSON.stringify(element)
            console.log(jsonData)
            return fetch('/upload', {
                method: "POST",
                body: jsonData,
                headers: {
                    'Content-Type': 'application/json',
                },
                mode:'cors'
            }).then(response => {
                if (response.ok) {
                    console.log(response)
                    console.log(element.id)
                    deleteSnapshotById('Image', 'Images', element.id)
                        .then(successMessage => {
                            console.log(successMessage);
                        })
                        .catch(error => {
                            console.log(error.message);
                        });
                }
            }).then(data => {
                console.log('Response data:', data);
            }).catch(error => {
                console.error('Error:', error);
            });
        });

        const responses = await Promise.all(fetchPromises);
        responses.forEach(response => console.log(response));

        console.log('Svi zapisi iz IndexedDB:', snapshots);
    } catch (error) {
        console.error(error);
    }
}

self.addEventListener('sync', (event)=>{
    console.log("syniyng")
    if(event.tag==="sync-snaps") {
        event.waitUntil(syncSnaps())
    }
})


self.addEventListener("notificationclick", (event) => {
    let notification = event.notification;
    notification.close();
    console.log("notificationclick", notification);
    event.waitUntil(
    clients
        .matchAll({ type: "window", includeUncontrolled: true })
        .then(function (clis) {
    if (clis && clis.length>0) {
        console.log('clis', clis)
        clis.forEach(async (client) => {
        await client.navigate(notification.data.redirectUrl);
        return client.focus();
    });
    } else if (clients.openWindow) {
        return clients
                .openWindow(notification.data.redirectUrl)
                .then((windowClient) =>
                windowClient ? windowClient.focus() : null);
                }
            })
        );
    });

self.addEventListener("push", function (event) {
    var data = { title: "title", body: "body", redirectUrl: "/" };
    if (event.data) {
        data = JSON.parse(event.data.text());
    }
    console.log(data)
    let options =
    {
        body: data.body,
        vibrate: [200, 100, 200, 100, 200, 100, 200],
        data: {
            redirectUrl: data.redirectUrl,
        },
    };
    event.waitUntil(
        self.registration.showNotification(data.title, options)
    );
});

self.addEventListener('notificationclose', function(event) {
    console.log("notificationclose", event)
})


