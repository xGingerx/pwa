if(("mediaDevices" in navigator)) {
    navigator.mediaDevices.
    getUserMedia({ video:true, audio:false}).
    then((stream)=> {
        player.srcObject=stream;
    }).catch((err)=>{
        alert("Video is not enabled for your version")
    })
} 

document.getElementById('last-button').addEventListener('click',async (event)=>{
    await fetchData()
})
async function fetchData() {
  try {
    const response = await fetch('/getData');
    var result;
    if(response){
        result = await response.json();
    } else {
        return
    }

    if(result.slika){
        var img = new Image()
        console.log(result.slika)
        img.onload = function(){
            const media = document.getElementById('player')
            var lastPic = document.getElementById('last-picture');
            lastPic.width=media.getBoundingClientRect().width
            lastPic.height=media.getBoundingClientRect().height
            lastPic.getContext("2d").drawImage(img, 0, 0, lastPic.width, lastPic.height)
            
        }
        img.onerror = function(){
            img.src='https://pwa-lsth.onrender.com/public/images/no.png'
            //alert("Slika se ne može ispravno prikazati jer nije ispravno prenesena kroz mrežu")
        }
        img.src=result.slika
    }
    
  } catch (error) {
    console.error('Error fetching data:', error);
  }
}

window.onload = fetchData

document.getElementById('take-pic').addEventListener('click', (event)=> {
    const picCanvas = document.getElementById('picture')
    const media = document.getElementById('player')
    picCanvas.width=media.getBoundingClientRect().width
    picCanvas.height=media.getBoundingClientRect().height
    picCanvas.getContext("2d").drawImage(player, 0, 0, picCanvas.width, picCanvas.height)

    if(document.getElementById('new-pic').firstChild){
        document.getElementById('new-pic').removeChild(document.getElementById('new-pic').firstChild)
    }
    var button = document.createElement('button')
    button.textContent='Upload'
    button.id='upload'
    if(!document.getElementById('button-upload').firstChild){
        document.getElementById('new-pic').appendChild(button)
    }
    
    document.getElementById('upload').addEventListener('click',(event)=>{
        // upload slike
        if('serviceWorker' in navigator && 'SyncManager' in window) {
            console.log("save in indexedDb")
            fetch(picCanvas.toDataURL())
                .then((res)=>res.blob())
                .then((blob)=>{
                    console.log("image is blob")
                    saveImageToIndexedDB(picCanvas.toDataURL())
                    return navigator.serviceWorker.ready
                }).then((swRegistration)=> {
                    return swRegistration.sync.register("sync-snaps")
                }).then(()=>{
                    console.log("Queued for sync")
                }).catch((err)=>{console.log(err)})
        }else{
            console.log("Preglednik ne podržava back sync")
        }
    })
})


function redirectOffline() {
    window.location.href = '/public/offline.html'
}

function redirectHome() {
    window.location.href = '/'
}

function saveImageToIndexedDB(blob) {
    var dbName = 'Image';
    var storeName = 'Images';
    var dbVersion = 1;
  
    return new Promise((resolve, reject) => {
        var request = window.indexedDB.open(dbName, dbVersion);
    
        request.onupgradeneeded = function (event) {
          var db = event.target.result;
          var store = db.createObjectStore(storeName, { keyPath: 'id', autoIncrement: true });
        };

        request.onsuccess = function (event) {
            var db = event.target.result;
      
            // Otvorite transakciju za pisanje
            var transaction = db.transaction([storeName], 'readwrite');
            var store = transaction.objectStore(storeName);
            // Dodajte sliku u object store
            var addRequest = store.add({ imageData: blob });
      
            addRequest.onsuccess = function (event) {
              console.log('Slika uspješno spremljena u IndexedDB.');
              resolve();
            };
            addRequest.onerror = function (event) {
                console.error('Greška pri spremanju slike u IndexedDB.');
                reject('Greška pri spremanju slike u IndexedDB.');
              };
            };
        
            request.onerror = function (event) {
              console.error('Greška pri otvaranju baze podataka.');
              reject('Greška pri otvaranju baze podataka.');
            };
    });
 
}