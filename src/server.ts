import express, { NextFunction } from 'express';
import path from 'path';
import { Request, Response } from 'express';
import fs from 'fs';
import dotenv from 'dotenv';
import https from 'https';
import {client, createTable} from './database'
import {entries,del} from 'idb-keyval'
import webpush from 'web-push'



dotenv.config()

const app = express();
const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

app.use(express.static(__dirname + '/public'))
app.use(express.static(__dirname + '/views'))
app.use(express.urlencoded({ extended: true, limit:'50mb' }));
app.use(express.json());
app.use(ignoreFavicon);

createTable()

app.get('/', (req: Request, res: Response) => {
  res.sendFile(__dirname + '/views/home.html')
});


function ignoreFavicon(req:Request, res:Response, next:any) {
  if (req.originalUrl === '/favicon.ico') {
      res.status(204).end();
  } else {
      next();
  }
}

app.get('/getData', async (req: Request, res: Response) => {
  try {
    const result = (await client.query(`SELECT max(id) as id, slika as slika FROM images group by slika`)).rows[0];
    console.log(result)
    const byteaData = result.slika.toString('base64')
    const dataURL = `data:image/png;base64,${byteaData}`;
    res.json({"slika": dataURL})
  } catch (error) {
    console.error('Error fetching data:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.post('/upload',async (req, res)=>{
    console.log("Pozvan upload POST metoda")
    const id = req.body.id;
    const data = req.body.imageData;
    const maxid= (await client.query("SELECT max(id) as max_id FROM images")).rows[0].max_id + 1

    try{
      await client.query(`INSERT INTO "images" VALUES ($1, $2)`, [maxid, data])
      console.log("succesfully entered in database")
    } catch(err){
      console.log(err)
    }
    await sendPushNotification("New picture")
    res.redirect('/')
})

async function sendPushNotification(snapTitle:any){
  const publicVapidKey = 'BIXFX7W-QB0fe9CerZJMXAUPXBQDIzuQnOAWtsxz39VDYgyHssc-uPYE8lELqS5ulZYksXDLb-2ddNtyUaTlJMI'
  const privateVapidKey = 'qT5n8di0ME2u9QRhkBIs5LavnzHougkMrmWW0KoPkOo'
  webpush.setVapidDetails('mailto:sisterselo5@gmail.com', publicVapidKey, privateVapidKey)
  let subscriptions = (await client.query('SELECT * FROM subscriptions')).rows;
  subscriptions.forEach(async (sub:any) => {
    try{
      sub = JSON.parse(sub.subscriber)
      await webpush.sendNotification(sub, JSON.stringify({
        title: 'There is new pic!',
        body:'Somebody uploaded their new pic',
        redirectURL:'/subscriber'
      }))
    } catch(error){
      console.error(error)
    }
  })
}
app.get('/subscriber', (req, res) => {
  res.sendFile(__dirname + '/views/home.html')
})

app.post('/saveSubscription', async function(req, res) {
  let sub =req.body.sub
  console.log(sub)
  sub = JSON.stringify(sub)
  try{
    await client.query(`INSERT INTO subscriptions VALUES($1)`, [sub])
  } catch(err) {
    console.log(err)
  }
  res.json({
    success:true
  })
})


if (externalUrl) {
  const hostname = '0.0.0.0';
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from outside on ${externalUrl}`);
  });
} else {
  https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
    }, app)
    .listen(port, function () {
    console.log(`Server running at https://localhost:${port}/`);
    });
}
