import { create } from 'domain';
import {Client} from 'pg'

export const client=new Client({
    host:process.env.DB_HOST,
    user:process.env.DB_USER,
    port:process.env.DB_PORT,
    password:process.env.DB_PASS,
    database:process.env.DB_DATABASE
})

const createImages =
`CREATE TABLE IF NOT EXISTS "images"(
    "id" INT PRIMARY KEY,
    "slika" BYTEA NOT NULL
)`

const deleteImage =
`DROP TABLE IF EXISTS "images";`

const createSubscriptions =
`CREATE TABLE IF NOT EXISTS "subscriptions"(
    "subscriber" VARCHAR
)`

const deleteSub =
`DROP TABLE IF EXISTS "subscriptions";`

export const createTable = async()=>{
    await client.connect();
    try{
        await client.query(createImages)
        await client.query(createSubscriptions)
    } catch(err) {
        console.log(err)
    }
}